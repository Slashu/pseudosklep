#pragma once

#include "Product.hpp"
#include <vector>

using namespace std;

class Cashier {
private:
    vector<Product> cashedProducts;
    double earnedMoney;

public:
    Cashier() {
        setEarnedMoney(0.00);
    }


public:
    void addCashedProduct(Product product) {

        cashedProducts.push_back(product);
        setEarnedMoney(earnedMoney + product.getPrice());
        //cout << product.getPrice() << endl;
        //cout << getEarnedMoney() << endl;
    }

    const vector<Product> getCashedProducts() {
        return cashedProducts;
    }

    void setCashedProducts(const vector<Product> &cashedProducts) {
        Cashier::cashedProducts = cashedProducts;
    }

    double getEarnedMoney() {
        return earnedMoney;
    }

    void setEarnedMoney(double earnedMoney) {
        Cashier::earnedMoney = earnedMoney;
    }


};