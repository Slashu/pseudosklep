#pragma once

#include <string>
#include <ostream>

using namespace std;

class Product {

private:
    string name;
    double price;
public:
    Product() {}

    Product(const string &name, double price) : name(name), price(price) {}

    const string &getName() const;

    void setName(const string &name);

    double getPrice() const;

    void setPrice(double price);

    friend ostream &operator<<(ostream &os, const Product &product);
};

const string &Product::getName() const {
    return name;
}

void Product::setName(const string &name) {
    Product::name = name;
}

double Product::getPrice() const {
    return price;
}

void Product::setPrice(double price) {
    Product::price = price;
}

ostream &operator<<(ostream &os, const Product &product) {
    os << "name: " << product.name << " price: " << product.price;
    return os;
}
