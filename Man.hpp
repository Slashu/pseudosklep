#pragma once

#include <string>

using namespace std;

class Man {
private:
    int age;
    int height;
    string sex;
    int ID;

public:
    Man() {}

    Man(int age, int height, const string &sex, int id);

    int getAge() const;

    void setAge(int age);

    int getHeight() const;

    void setHeight(int height);

    const string &getSex() const;

    void setSex(const string &sex);

    int getId() const;

    void setId(int id);
};

Man::Man(int age, int height, const string &sex, int id) : age(age), height(height), sex(sex), ID(id) {}

int Man::getAge() const {
    return age;
}

void Man::setAge(int age) {
    Man::age = age;
}

int Man::getHeight() const {
    return height;
}

void Man::setHeight(int height) {
    Man::height = height;
}

const string &Man::getSex() const {
    return sex;
}

void Man::setSex(const string &sex) {
    Man::sex = sex;
}

int Man::getId() const {
    return ID;
}

void Man::setId(int id) {
    ID = id;
}
