#pragma once

#include "Man.hpp"
#include "Product.hpp"
#include <string>
#include <vector>

using namespace std;

class Female : public Man {
private:
    string name;
    string surname;

private:
    vector<Product> basket;

public:
    Female() {}

    Female(int age, int height, const string &sex, int id, const string &name, const string &surname) : Man(age, height,
                                                                                                            sex, id),
                                                                                                        name(name),
                                                                                                        surname(surname) {}

public:
    void addToBasket(Product product) {
        basket.push_back(product);
    }

public:
    Product pushLast() {
        return basket.back();
    }

public:
    void removeFromBasket() {
        basket.pop_back();
    }

    const vector<Product> &getBasket() const {
        return basket;
    }
};