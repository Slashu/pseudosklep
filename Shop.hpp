#pragma once

#include <vector>
#include "Product.hpp"
#include "Cashier.hpp"
#include "Female.hpp"
#include <random>
#include <algorithm>

using namespace std;

class Shop {
private:
    vector<Product> VProducts;
    int vectorSize;
    string alphabet = "abcdefghijklmnoprstuwyz";
    double price;
    string name;
    default_random_engine randomEngine;
    int basketSize;

public:
    Shop() {
        populateShop();
    }

public:
    Product randomProduct() {
        vectorSize = VProducts.size();
        uniform_int_distribution<int> distribution(0, vectorSize);
        return VProducts.at(distribution(randomEngine));
    }

private:
    void populateShop() {
        randomEngine.seed(time(0));
        uniform_real_distribution<double> randDouble(1, 10);
        for (int i = 0; i < 50; ++i) {
            shuffle(alphabet.begin(), alphabet.end(), randomEngine);
            name.append(alphabet.substr(0, 3));
            price = randDouble(randomEngine);
            VProducts.push_back(Product(name, price));
            name.clear();
        }
    }

public:
    void goToCashier(Female &female, Cashier &cashier) {

        while (!female.getBasket().empty()) {
            cash(female, cashier);
        }
    }

public:
    void goToCashier(Male &male, Cashier &cashier) {
        while (!male.getBasket().empty()) {
            cash(male, cashier);
        }

    }

private:
    void cash(Female &female, Cashier &cashier) {
        cashier.addCashedProduct(female.pushLast());
        female.removeFromBasket();
    }

private:
    void cash(Male &male, Cashier &cashier) {
        cashier.addCashedProduct(male.pushLast());
        male.removeFromBasket();
    }
};