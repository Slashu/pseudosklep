#include <iostream>
#include "Male.hpp"
#include "Shop.hpp"

using namespace std;

int main() {
/*
    default_random_engine defaultRandomEngine;
    uniform_int_distribution<int> distribution(0,10);

   *//* for (int i = 0; i < 20; ++i) {
        cout << distribution(defaultRandomEngine) << endl;
    }*//*
   */


    Male male = Male();
    Female female = Female();
    Shop shop = Shop();
    Cashier cashier = Cashier();
    Product rProduct;
    for (int i = 0; i < 15; ++i) {
        rProduct = shop.randomProduct();
        male.addToBasket(rProduct);
        rProduct = shop.randomProduct();
        female.addToBasket(rProduct);
        //cout << rProduct.getName() << " " << rProduct.getPrice() << endl;

    }

    shop.goToCashier(female, cashier);
    shop.goToCashier(male, cashier);
    cout << "earned money = " << cashier.getEarnedMoney() << endl;

}
